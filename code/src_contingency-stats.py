########################################################################
#
# Program:   contingency-stats.py
#
# Copyright (c) 2021 Philippe P. PEBAY
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither name of Philippe PEBAY nor the names
#   of any contributors may be used to endorse or promote products derived
#   from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#########################################################################
import sys
import os
import getopt
import csv
import math
import numpy as np
import scipy.stats as ss

if __name__ == '__main__':
    """Main routine
    """

    # Default parameter values
    file_name = '.../output.csv'
    var_X_name = 'X'
    var_Y_name = 'Y'
    separator = ','

    # Try to hash command line with respect to allowable flags
    try:
        opts, args = getopt.getopt(sys.argv[1:], "f:x:y:s:")
    except getopt.GetoptError:
        print("** ERROR: incorrect command line parameter(s). Exiting.")
        sys.exit(1)

    # Parse arguments and assign corresponding variables
    for o, a in opts:
        if o == "-f":
            file_name = a
        elif o == "-x":
            var_X_name = a
        elif o == "-y":
            var_Y_name = a
        elif o == "-s":
            separator = a
    if not file_name:
        print("** ERROR: file name required. Exiting.")
        sys.exit(1)

    # Try to parse specified input file
    first_row = True
    columns_to_outcomes = None
    cardinalities = {}
    n_per_row = 0
    try:
        with open(file_name, newline='') as f:
            reader = csv.reader(f, delimiter=separator, quotechar='#')
            for row in reader:
                # Retrieve second variable outcomes from first line
                if first_row:
                    columns_to_outcomes = {i: s for i, s in enumerate(row)}
                    n_per_row = len(columns_to_outcomes) + 1
                    first_row = False

                # Then parse and check all other lines
                else:
                    # Sanity check
                    if len(row) != n_per_row:
                        print("** ERROR: row has {} entries instead of {}. Exiting.".format(
                            len(row), n_per_row))
                        sys.exit(1)

                    # Append cardinalities for second variable
                    cardinalities[row[0]] = {
                        columns_to_outcomes[i]: int(s) for i, s in enumerate(row[1:])}

    # Bail out if parsing failed
    except:
        print("** ERROR: problem occurred when trying to parse {}. Exiting.".format(
            file_name))
        sys.exit(1)

    # Bail out if empty distribution
    if not cardinalities:
        print("** ERROR: empty distribution. Exiting.".format(
            file_name))
        sys.exit(1)

    # Compute total cardinality
    n_obs = sum([sum(v.values()) for v in cardinalities.values()])
    print("# Retrieved grand total of {} cardinalities in ({}, {}) contingency table".format(
        n_obs, var_X_name, var_Y_name))

    # Compute empirical joint probability mass function
    ejpmf = {x: {y: n_xy / n_obs
                 for y, n_xy in n_x.items()} for x, n_x in cardinalities.items()} 

    # Compute marginal distributions
    epmf_X, epmf_Y = {}, {}
    for x, f_x in ejpmf.items():
        epmf_X[x] = 0
        for y, f_xy in f_x.items():
            epmf_X[x] += f_xy
            epmf_Y[y] = epmf_Y.get(y, 0) + f_xy

    # Sanity check
    if abs(sum(epmf_X.values()) - 1) > 1e-8:
        print("** ERROR: marginal EPMF of {} does not sum to 1. Exiting.".format(
            var_X_name))
        sys.exit(1)
    n_X = len(epmf_X)
    print("# Computed marginal EPMF of {} with {} outcomes".format(
        var_X_name, n_X))
    if abs(sum(epmf_Y.values()) - 1) > 1e-8:
        print("** ERROR: marginal EPMF of {} does not sum to 1. Exiting.".format(
            var_Y_name))
        sys.exit(1)
    n_Y = len(epmf_Y)
    print("# Computed marginal EPMF of {} with {} outcomes".format(
        var_Y_name, n_Y))

    # Compute chi-squared statistic
    chi_2 = 0
    for x, n_x in cardinalities.items():
        for y, p_xy in n_x.items():
            expected = epmf_X[x] * epmf_Y[y] * n_obs
            delta = cardinalities[x][y] - expected
            chi_2 += delta * delta / expected
    print("# Chi-squared = {}, p-value = {}".format(
          chi_2, 1 - ss.chi2.cdf(chi_2, df = (n_X - 1) * (n_Y - 1) )))

    # Compute individual information entropies
    H_X = 0
    for x, p in epmf_X.items():
        H_X -= p * math.log(p)
    print("# Information entropy of {} = {}".format(
        var_X_name, H_X))
    H_Y = 0
    for y, p in epmf_Y.items():
        H_Y -= p * math.log(p)
    print("# Information entropy of {} = {}".format(
        var_Y_name, H_Y))

    # Compute mutual information entropies
    H_XY, H_Y_X, H_X_Y = 0, 0, 0
    for x, p_x in ejpmf.items():
        for y, p_xy in p_x.items():
            if abs(p_xy) > 1e-323:
                H_XY -= p_xy * math.log(p_xy)
                H_Y_X -= p_xy * math.log(p_xy / epmf_Y[y])
                H_X_Y -= p_xy * math.log(p_xy / epmf_X[x])
    I_XY = H_XY - H_Y_X - H_X_Y
    print("# Joint information entropy of {} and {} = {}".format(
        var_X_name, var_Y_name, H_XY))
    print("# Conditional information entropy of {} given {} = {}".format(
        var_Y_name, var_X_name, H_Y_X))
    print("# Conditional information entropy of {} given {} = {}".format(
        var_X_name, var_Y_name, H_X_Y))
    print("# Mutual information of {} and {} = {}".format(
        var_X_name, var_Y_name, I_XY))
    print("# Normalized mutual information of {} and {} = {}".format(
        var_X_name, var_Y_name, I_XY / math.sqrt(H_X * H_Y)))
