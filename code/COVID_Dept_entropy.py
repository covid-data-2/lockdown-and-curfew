########################################################################
#
# Program:   Autocorrelation.py
#
# Copyright (c) 2021 Philippe P. PEBAY, Jean-Luc CAUT, Edouard LANSIAUX, Joachim FORGET
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither name of Philippe PEBAY nor the names
#   of any contributors may be used to endorse or promote products derived
#   from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#########################################################################

# -*- coding: utf-8 -*-

from pandas import read_csv
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set(style='whitegrid', context='notebook')

path='.../DeptMetro_CF_31Jan_PopR0_VersionDatesNorm.csv'

df = read_csv(path,header=0, sep=';') 
df.columns = ['date', 'Dept', 'CF8pm', 'CF6pm', 'Mask_transp', 'Mask_Closed', 'Mask_business', 'NSE', 'Lockdown', 'ICU_adm', 'Hospit', 'R(t)Mean', 'ICU_adm_conso']

cols = ['Dept', 'CF8pm', 'CF6pm', 'Mask_transp', 'Mask_Closed', 'Mask_business', 'NSE', 'Lockdown', 'ICU_adm', 'ICU_adm_conso', 'Hospit', 'R(t)Mean']
cm = np.corrcoef(df[cols].values.T)
cm[0][1] = cm[1][0] = 0.0028969492711759576
cm[0][2] = cm[2][0] = 0.0028969492711759576
cm[0][3] = cm[3][0] = 8.032503631824618e-15
cm[0][4] = cm[4][0] = 1.57525205718664e-15
cm[0][5] = cm[5][0] = - 6.76331270596425e-15
cm[0][6] = cm[6][0] = 0.00026056253399551114
cm[0][7] = cm[7][0] = 2.497791044231389e-06
cm[0][8] = cm[8][0] = 0.15787379052099693
cm[0][9] = cm[9][0] = 0.15787379052099587
cm[0][10] = cm[10][0] = 0.252556170763782
cm[0][11] = cm[11][0] = 0.6383841223300116
cm[1][2] = cm[2][1] = 0.022788786086373376
cm[1][3] = cm[3][1] = 0.06485795668421826
cm[1][4] = cm[4][1] = 0.13104768858109614
cm[1][5] = cm[5][1] = 0.19228152850072208
cm[1][6] = cm[6][1] = 0.0429010049100549
cm[1][7] = cm[7][1] = 0.089871922954477882
cm[1][8] = cm[8][1] = 0.03520320949708259
cm[1][9] = cm[9][1] = 0.03520320949708181
cm[1][10] = cm[10][1] = 0.043682441834632546
cm[1][11] = cm[11][1] = 0.18452315859297194
cm[2][3] = cm[3][2] = 0.0387627046452141
cm[2][4] = cm[4][2] = 0.07778601026191977
cm[2][5] = cm[5][2] = 0.1133197782892759
cm[2][6] = cm[6][2] = 0.09492365378267306
cm[2][7] = cm[7][2] = 0.05358107381802295
cm[2][8] = cm[8][2] = 0.02819501479428037
cm[2][9] = cm[9][2] = 0.028195014794288518
cm[2][10] = cm[10][2] = 0.04355680734107177
cm[2][11] = cm[11][2] = 0.14192388716280413
cm[3][4] = cm[4][3] = 0.379609175662248
cm[3][5] = cm[5][3] = 0.252554406346521
cm[3][6] = cm[6][3] = 0.06871104111273123
cm[3][7] = cm[7][3] = 0.2369010085713944
cm[3][8] = cm[8][3] = 0.025395038905818016
cm[3][9] = cm[9][3] = 0.025395038905816417
cm[3][10] = cm[10][3] = 0.05558594748847379
cm[3][11] = cm[11][3] = 0.21000145818802327
cm[4][5] = cm[5][4] = 0.5588055218665169
cm[4][6] = cm[6][4] = 0.005119163228445844
cm[4][7] = cm[7][4] = 0.02071181644632691
cm[4][8] = cm[8][4] = 0.01455389084340278
cm[4][9] = cm[9][4] = 0.014553890843401397
cm[4][10] = cm[10][4] = 0.03564354749538383
cm[4][11] = cm[11][4] = 0.25240993331629435
cm[5][6] = cm[6][5] = 0.023758527064609385
cm[5][7] = cm[7][5] = 2.4507148399803806e-05
cm[5][8] = cm[8][5] = 0.051978050840668734
cm[5][9] = cm[9][5] = 0.051978050840678656
cm[5][10] = cm[10][5] = 0.05363670964864339
cm[5][11] = cm[11][5] = 0.26208882115699766
cm[6][7] = cm[7][6] = 0.404110854327113
cm[6][8] = cm[8][6] = 0.12523580918224067
cm[6][9] = cm[9][6] = 0.12523580918225055
cm[6][10] = cm[10][6] = 0.09663566617001228
cm[6][11] = cm[11][6] = 0.2516705519099043
cm[7][8] = cm[8][7] = 0.08101433599173276
cm[7][9] = cm[9][7] = 0.08101433599172862
cm[7][10] = cm[10][7] = 0.05368176632998931 
cm[7][11] = cm[11][7] = 0.23725052248788145
cm[8][9] = cm[9][8] = 0.9999999999999996
cm[8][10] = cm[10][8] = 0.3512205619841403
cm[8][11] = cm[11][8] = 0.6011043959076754
cm[9][10] = cm[10][9] = 0.3512205619841403
cm[9][11] = cm[11][9] = 0.6011043959076754
cm[10][11] = cm[11][10] = 0.7436754900177638
sns.set(font_scale=0.3)
hm = sns.heatmap(cm, annot=True, cbar=True, linewidths=.5, square=True, cmap='BuPu', 
                 fmt='.2f', annot_kws={'size': 7}, yticklabels=cols, xticklabels=cols)

hm.set_yticklabels(cols, rotation=0)
fig=plt.figure(1)
fig.patch.set_facecolor('white') 
plt.title('Figure 3. NPIs efficiency independent assessment in metropolitan France: standardized mutual information method', fontsize=7, color='Blue')
plt.show()
