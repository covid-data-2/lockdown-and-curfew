########################################################################
#
# Program:   Autocorrelation.py
#
# Copyright (c) 2021 Philippe P. PEBAY, Jean-Luc CAUT, Edouard LANSIAUX, Joachim FORGET
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither name of Philippe PEBAY nor the names
#   of any contributors may be used to endorse or promote products derived
#   from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#########################################################################

from pandas import read_csv
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set(style='whitegrid', context='notebook')

path='.../DeptMetro_CF_31Jan_PopR0_VersionDatesNorm.csv'

df = read_csv(path, header=0, sep=';') 
df.columns = ['date', 'Dept', 'CF8pm','CF6pm', 'Mask_transp', 'Mask_Closed', 'Mask_Business', 'NSE', 'Lockdown', 'ICU_adm', 'Hospit', 'Population', 'R(t)Mean', 'ICU_adm_conso']

cols = ['Dept', 'CF8pm','CF6pm', 'Mask_transp', 'Mask_Closed', 'Mask_Business', 'NSE', 'Lockdown',  'ICU_adm', 'ICU_adm_conso', 'Hospit', 'R(t)Mean']
cm = np.corrcoef(df[cols].values.T)
sns.set(font_scale=0.3)
hm = sns.heatmap(cm, annot=True, cbar=True, linewidths=.5, square=True, cmap='BuPu', 
                 fmt='.2f', annot_kws={'size': 8}, yticklabels=cols, xticklabels=cols)

hm.set_yticklabels(cols, rotation=0)
fig=plt.figure(1)
fig.patch.set_facecolor('white') 
plt.title('Figure 2. NPIs efficiency independent assessment in metropolitan France: Pearson method', fontsize=8, color='Blue')
plt.show()
