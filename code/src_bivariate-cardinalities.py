########################################################################
#
# Program:   bivariate-cardinalities.py
#
# Copyright (c) 2021 Philippe P. PEBAY
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither name of Philippe PEBAY nor the names
#   of any contributors may be used to endorse or promote products derived
#   from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#########################################################################
import sys
import getopt
import math


def parse_command_line():
    """Parse command line arguments
    """

    # Default parameter values
    file_name = None
    col_X = None
    col_Y = None
    lag_Y_on_X = 0
    n_X = 0
    n_Y = 0
    separator = ','

    # Try to hash command line with respect to allowable flags
    try:
        opts, args = getopt.getopt(sys.argv[1:], "f:l:p:q:s:x:y:")
    except getopt.GetoptError:
        print("** ERROR: incorrect command line parameter(s). Exiting.")
        sys.exit(1)

    # Parse arguments and assign corresponding variables
    for o, a in opts:
        if o == "-f":
            file_name = a
        elif o == "-x":
            try:
                col_X = int(a)
            except:
                pass
        elif o == "-y":
            try:
                col_Y = int(a)
            except:
                pass
        elif o == "-l":
            try:
                lag_Y_on_X = int(a)
            except:
                pass
        elif o == "-p":
            try:
                n_X = int(a)
            except:
                pass
        elif o == "-q":
            try:
                n_Y = int(a)
            except:
                pass
        elif o == "-s":
            separator = a
    if not col_X:
        print("** ERROR: nonzero olumn index for variable X required. Exiting.")
        sys.exit(1)
    if not col_Y:
        print("** ERROR: nonzero column index for variable Y required. Exiting.")
        sys.exit(1)
    if lag_Y_on_X < 0:
        print("** ERROR: Y on X lag must be nonnegative. Exiting.")
        sys.exit(1)
    if n_X < 0:
        print("** ERROR: X quantization must be nonnegative. Exiting.")
        sys.exit(1)
    if n_Y < 0:
        print("** ERROR: Y quantization must be nonnegative. Exiting.")
        sys.exit(1)
    if not file_name:
        print("** ERROR: file name required. Exiting.")
        sys.exit(1)

    return file_name, col_X, col_Y, lag_Y_on_X, n_X, n_Y, separator


def compute_cardinalities(file_name, col_X, col_Y, lag_Y_on_X, separator):
    """Get cardinalities from data file
    """

    # Try to parse data file
    try:
        f = open(file_name, 'r')
        lines = f.readlines()
        f.close()
    except:
        print("** ERROR: could not open {}. Exiting.".format(file_name))
        sys.exit(1)

    # Retrieve names of specified variable columns from first line
    names = lines[0].split(separator)
    if col_X < 0 or col_X >= len(names):
        print("** ERROR: incorrect column index for variable X: {}. Exiting.".format(
            col_X))
        sys.exit(1)
    name_X = names[col_X].strip()
    if col_Y < 0 or col_Y >= len(names):
        print("** ERROR: incorrect column index for variable Y: {}. Exiting.".format(
            col_Y))
        sys.exit(1)
    name_Y = names[col_Y].strip()

    # Retrieve bivariate observations over relevant data lines
    cardinalities = {}
    for l in lines[1+lag_Y_on_X:-lag_Y_on_X if lag_Y_on_X else None]:
        data = l.split(separator)
        i1 = int(data[col_X].strip())
        i2 = int(data[col_Y].strip())
        cards = cardinalities.get(i1, {})
        cards[i2] = cards.get(i2, 0) + 1
        cardinalities[i1] = cards

    # Perform sanity check on grand total
    n_obs = len(lines) - 1 - 2 * lag_Y_on_X
    if n_obs != sum([sum(
        [c for c in d.values()]) for d in cardinalities.values()]):
        print("** ERROR: incorrect grand total <>", n_obs)
    else:
        print("# Grand total:", n_obs)

    # Return variable names and values
    return name_X, name_Y, cardinalities


def main():
    """Main routine
    """

    # Parse command-line arguments
    file_name, col_X, col_Y, lag_Y_on_X, n_X, n_Y, separator = parse_command_line()
    
    # Retrieve data cardinalities from file
    name_X, name_Y, cardinalities = compute_cardinalities(
        file_name, col_X, col_Y, lag_Y_on_X, separator)
    print("# X variable name:", name_X)
    print("# Y variable name:", name_Y)

    # Determine and save all Y outcomes
    if n_Y:
        # Quantize Y
        out_Y = range(n_Y)
        min2 = min([min(v.keys()) for v in cardinalities.values()])
        max2 = max([max(v.keys()) for v in cardinalities.values()]) * 1.01
        sz2 = (max2 - min2) / n_Y
    else:
        out_Y = set()
        for vals in cardinalities.values():
            out_Y.update(vals.keys())

    # Open output file and save all Y outcomes
    f = open("./{}-{}.dat".format(name_X, name_Y), "w+")
    f.write(','.join([str(i) for i in out_Y]))
    f.write('\n')

    if n_X:
        # Quantize X
        quantized = {}
        min1 = min(cardinalities.keys())
        max1 = max(cardinalities.keys()) * 1.01
        sz1 = (max1 - min1) / n_X
        if n_Y:
            # Quantize both cardinalities
            for i1, d1 in cardinalities.items():
                for i2, c in d1.items():
                    k1 = int((i1 - min1) // sz1)
                    k2 = int((i2 - min2) // sz2)
                    q1 = quantized.get(k1, {})
                    q1[k2] = q1.get(k2, 0) + c
                    quantized[k1] = q1

        else:
            # Do not quantize Y
            for i1, d1 in cardinalities.items():
                for i2, c in d1.items():
                    k1 = int((i1 - min1) // sz1)
                    q1 = quantized.get(k1, {})
                    q1[i2] = q1.get(i2, 0) + c
                    quantized[k1] = q1

        # Save bivariate quantized cardinalities
        for i1 in range(n_X):
            if sum([quantized.get(i1, {}).get(oy, 0) for oy in out_Y]):
                f.write(
                    ','.join(
                        [str(i1)] + [str(quantized.get(i1, {}).get(oy, 0)) for oy in out_Y]))
                f.write('\n')

    else:
        # No quantization for X
        if n_Y:
            # Quantize Y only
            quantized = {}
            for i1, d1 in cardinalities.items():
                for i2, c in d1.items():
                    k2 = int((i2 - min2) // sz2)
                    q1 = quantized.get(i1, {})
                    q1[k2] = q1.get(k2, 0) + c
                    quantized[i1] = q1
            for k in quantized.keys():
                f.write(','.join(
                    [str(k)] + [str(quantized[k].get(oy, 0)) for oy in out_Y]))
                f.write('\n')
            
        else:
            # No quantization for either variable
            for k in cardinalities.keys():
                f.write(','.join(
                    [str(k)] + [str(cardinalities[k].get(oy, 0)) for oy in out_Y]))
                f.write('\n')

    # Close output file
    f.close()

if __name__ == '__main__':
    # Execute main routine
    main()
