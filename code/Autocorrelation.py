########################################################################
#
# Program:   Autocorrelation.py
#
# Copyright (c) 2021 Philippe P. PEBAY, Jean-Luc CAUT, Edouard LANSIAUX, Joachim FORGET
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither name of Philippe PEBAY nor the names
#   of any contributors may be used to endorse or promote products derived
#   from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#########################################################################

from pandas import read_csv,DataFrame
from statsmodels.graphics.tsaplots import plot_acf,plot_pacf
import matplotlib.pyplot as plt
import numpy as np


path='F:/BigData-MachineLearning/Covid/'

'''

df = DataFrame(read_csv(path+'Dept_CF.csv',header=0, sep=';') )
data = df.drop(['Dept', 'Pop', 'CF8pm','CF6pm', 'Mask_transp', 'Mask_Closed', 'Mask_Business', 'NSE', 'Lockdown','ICU_adm','Hospit', 'R0'],axis=1)

#Warning only works if the date format is the next: YYYY-MM-DD
def f(args):
    return args.loc[:,'ICU_adm'].sum()           
# la fonction reste_index permet d'ajouter le nom de la deuxième colonne
df1=data.loc[:,['date','ICU_adm']].groupby('date').apply(f).reset_index(name='ICU_adm')

path_out='F:/BigData-MachineLearning/Covid/Dept_CF1.csv'

print(df1)
df1.to_csv(path_out, index=False, header=True, sep=';')

'''
df1 = read_csv(path+'Dept_CF1.csv',header=0, sep=';') 

df1=df1.drop(['date'],axis=1)
data_a = df1.to_numpy().T[0]

plot_acf(data_a, lags=320, fft=False, alpha=0.05, title='ICU Autocorrelation')
plot_pacf(data_a,lags=160, alpha=0.05, title='ICU Partial Autocorrelation')

plt.show()
